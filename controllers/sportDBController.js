console.log('Working');
const app = angular.module('sportDBApp', []);

app.controller('sportDBController', ($scope, $http) => {
  $http.get('http://www.thesportsdb.com/api/v1/json/1/searchplayers.php?t=Chelsea')
  .then((res) => {
    const player = $scope.player = res.data;

    $scope.favorite = [];

    // Get details players and append them
    $scope.getDetail = function(thumb, fullNamePlayer, descriptionEN ){
      $scope.imgThumb = thumb;
      $scope.fullNamePlayer = fullNamePlayer;
      $scope.playerDescriptionEN =  descriptionEN;
      $scope.message = '';
    }

    $scope.addToArray = function(fullNamePlayer){
      if ($scope.favorite.indexOf(fullNamePlayer) == -1) {
        $scope.favorite.push(fullNamePlayer);
        $scope.message = '';
      } else {
        $scope.message = 'Player exists already!';
      }
    }

    $scope.removeFromArray = function(index){
      $scope.favorite.splice(index, 1);
      $scope.message = '';
    }

    // $scope.checkErr() = function() {
    // }
  });
});
