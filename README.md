# SportsDB-App

SportDB App is an app that allows you to see football teams and their football players plus you can add your favorite players on your board.

## Getting Started

For running the app I used atom-live-server(Which is a plug-in of ATOM editor) that allows you to run the app on localhost.

### Tech

Angularjs 1.6.7

### Styling

Bootstrap 4.0.0

### Icons

fontawesome v5.0.6

### Text-Editor
Atom 